package com.abby.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.abbyy.parser.model.Flag;
import com.abbyy.parser.model.Structure;
import com.abbyy.parser.model.Type;

/**
 * Class starting application
 *
 * @author Vladimir Malakhov
 */
public class Application {

	private static Scanner scanner;
	private static List<Structure> list = new ArrayList<Structure>();
	private static String query;

	public static void main(String[] args) {
		scanner = new Scanner(System.in);
		System.out.println("Введите строку:");
		query = scanner.nextLine();
		while (query.equals("")) {
			System.out.println(" Введите не пустую строку! ");
			query = scanner.nextLine();
		}
		query = query.replaceAll(" ", "  ");
		query = "  " + query + " ";
		parseString();
		if (list.size() != 0) {
			System.out.println(" Результат: " + list);
		} else {
			System.out.println("Пустой результат - введите верную строку! ");
		}
	}

	/**
	 * Method that parses input
	 */
	private static void parseString() {
		// pattern for semantic types
		Pattern semPattern = Pattern.compile("\\s([+|-]?)\\{([а-я0-9\\s]+)\\}\\s");
		putStructure(semPattern, Type.SEMANTIC);

		// pattern for exact full text types
		Pattern exMatchPattern = Pattern.compile("\\s([+|-]?)\\«([а-я0-9\\s]+)\\»\\s");
		putStructure(exMatchPattern, Type.EXACT_FULL_TEXT);

		// pattern for full text types
		Pattern exactPattern = Pattern.compile("\\s([+|-]?)([а-я0-9]+)\\s");
		putStructure(exactPattern, Type.FULL_TEXT);

		// pattern for wildcard text types
		Pattern wildcardPattern = Pattern.compile("\\s([+|-]?)([а-я[\\*|\\?]]+)\\s");
		putStructure(wildcardPattern, Type.WILDCARD);

		// pattern for semantic class types
		Pattern semClassPattern = Pattern.compile("\\s([+|-]?)([А-ЯA-Z0-9[_|*]]+)\\s");
		putStructure(semClassPattern, Type.SEMANTIC_CLASS);
	}

	/**
	 * Method that creates and puts the structure into the list
	 */
	private static void putStructure(Pattern pattern, Type type) throws IndexOutOfBoundsException {
		Matcher m = pattern.matcher(query);
		// temp Strings concatenating according to groups
		String temp_mustnot = "";
		String temp_must = "";
		String temp_none = "";
		while (m.find()) {
			if (m.group(1).equals("+")) {
				temp_must = temp_must + " " + m.group(2);
			} else if (m.group(1).equals("-")) {
				temp_mustnot = temp_mustnot + " " + m.group(2);
			} else {
				temp_none = temp_none + " " + m.group(2);
			}
			if (type == Type.SEMANTIC) {
				query = query.replace(m.group(1) + "{" + m.group(2) + "}", "");
			} else if (type == Type.EXACT_FULL_TEXT) {
				query = query.replace(m.group(1) + "«" + m.group(2) + "»", "");
			} else if (type == Type.SEMANTIC_CLASS || type == Type.FULL_TEXT || type == Type.WILDCARD) {
				query = query.replace(m.group(1) + m.group(2), "");
			}
		}
		if (!temp_none.equals("") && !temp_none.equals(" ")) {
			Structure structure = new Structure(temp_none.replaceAll("  ", " "), type, Flag.NONE);
			list.add(structure);
		}
		if (!temp_must.equals("") && !temp_must.equals(" ")) {
			Structure structure = new Structure(temp_must.replaceAll("  ", " "), type, Flag.MUST);
			list.add(structure);
		}
		if (!temp_mustnot.equals("") && !temp_mustnot.equals(" ")) {
			Structure structure = new Structure(temp_mustnot.replaceAll("  ", " "), type, Flag.MUST_NOT);
			list.add(structure);
		}
	}
}
