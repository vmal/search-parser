package com.abbyy.parser.model;

/**
 * Type enum class for Structure
 *
 * @author Vladimir Malakhov
 */
public enum Type {
    SEMANTIC("Семантический запрос"),
    FULL_TEXT("Полнотекстовый запрос"),
    EXACT_FULL_TEXT("Полнотекстовый запрос - точное совпадение"),
    SEMANTIC_CLASS("Семантические классы"),
    WILDCARD("Wildcard");

    private String description;

    public String getDescription() {
		return description;
	}

	private Type(String description) {
        this.description = description;
    }
}

