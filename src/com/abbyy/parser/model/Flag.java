package com.abbyy.parser.model;

/**
 * Flag enum class for Structure
 *
 * @author Vladimir Malakhov
 */
public enum Flag {
	MUST("MUST"), MUST_NOT("MUST_NOT"), NONE("NONE");
	private String description;

	public String getDescription() {
		return description;
	}

	private Flag(String description) {
		this.description = description;
	}
}
