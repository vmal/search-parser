package com.abbyy.parser.model;

/**
 * Structure class
 *
 * @author Vladimir Malakhov
 */
public class Structure {
	
	private String text;
	private Type type;
	private Flag flag;
	
	public Structure(String text, Type type, Flag flag){
		this.text = text;
		this.type = type;
		this.flag = flag;
	}

	@Override
	public String toString(){
		return "Тип: " + type.getDescription() + ";" +
			   " Текст: " + text + ";" +
			   " Флаг: " + flag.getDescription();
	}
}
