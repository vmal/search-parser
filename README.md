# Search parser project    developed by Vladimir Malakhov.

Application runs on java 8. Main starting class is com.abby.parser.Application.
The input string should have " " (spaces) between different types. Mind that "-" and "–" two different symbols - works only with "-" (minus) for MUST_NOT flags.
In case of wrong input or emty result you will get "Пустой результат - введите верную строку!" message.

## Egs of wrong input:
* }рыбалка{
* {рыбалка{{зимняя}
* {}
* рыба?ка!
* рыбалкаFISH*
* {fdsgsf} - works only for cyrillic, doesnt allow english for sematic, full texts, wildcard
